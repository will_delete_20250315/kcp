# kcp

> kcp 是一种 ARQ 协议,可解决在网络拥堵情况下 tcp 协议的网络速度慢的问题

![](screenshot/kcp.gif)

## 下载安装

直接在 OpenHarmony-SIG 仓中搜索 kcp 并下载。

## 使用说明

准备一套完整的 OpenHarmony 3.1 Beta 代码

1. 库代码存放路径：./third_party/kcp

2. 修改添加依赖的编译脚本

   在/developtools/bytrace_standard/ohos.build 文件中添加以下修改：

   ```
   {
     "subsystem": "developtools",
     "parts": {
       "bytrace_standard": {
         "module_list": [
           "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
           "//developtools/bytrace_standard/bin:bytrace_target",
           "//developtools/bytrace_standard/bin:bytrace.cfg",
           "//developtools/bytrace_standard/interfaces/kits/js/napi:bytrace",
           "//third_party/kcp:kcp_targets"
         ],
         "inner_kits": [
           {
             "type": "so",
             "name": "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
             "header": {
               "header_files": [
                 "bytrace.h"
               ],
               "header_base": "//developtools/bytrace_standard/interfaces/innerkits/native/include"
             }
           }
         ],
          "test_list": [
           "//developtools/bytrace_standard/bin/test:unittest"
         ]
       }
     }
   }
   ```

3. 用命令 ./build.sh --product-name rk3568 --ccache 编译

4. 生成库文件路径：

   out/rk3568/common/common

   该路径会生成 test 可执行文件

## 接口说明

1.  接收到下层协议 UDP 传进来的数据底层数据 buffer 转换成 kcp 的数据包格式
    `int ikcp_input(ikcpcb *kcp, const char *data, long size)`
    KCP 报文分为 ACK 报文、数据报文、探测窗口报文、响应窗口报文四种。
    kcp 报文的 una 字段（snd_una：第一个未确认的包）表示对端希望接收的下一个 kcp 包序号，也就是说明接收端已经收到了所有小于 una 序号的 kcp 包。解析 una 字段后需要把发送缓冲区里面包序号小于 una 的包全部丢弃掉

2.  用户层面的数据读取

    `ikcp_recv(ikcpcb *kcp, char *buffer, int len)`

    首先合并 fragment，如果 rcv_queue 小于 rcv_wnd(接收窗口大小)，则将 rcv_buf 中合适的 segment 放入 rcv_queue 中

3.  将 buffer 中的数据发送，把要发送的 buffer 分片成 KCP 的数据包格式，插入待发送队列中

    `ikcp_send(ikcpcb *kcp, const char *buffer, int len) `

    当用户的数据超过一个 mss(最大分片大小)的时候，会对发送的数据进行分片处理。KCP 采用的是流的方式进行分片处理

    如果需要发送的数据大小大于 mss，则将其拆分为多个 segment 发送，将其 frg 至为其相应的序号，序号从 count-1 开始递减至 0，即 count-1 表示第一个 segment，0 表示最后一个 segment。

4.  刷新待处理数据，待处理数据包括 ack，win probe，push data 等等，以及检测 snd_buf 中的数据是否需要重传

    `ikcp_flush(ikcpcb *kcp)`

## 约束与限制

在下述版本验证通过：DevEco Studio: 3.1 Beta1，OpenHarmony SDK: API9。

## 目录结构

```
|---- kcp
|     |---- ikcp.c   #kcp的主要实现逻辑
|     |---- test.c   #测试代码文件
|     |---- screenshot   #测试结果图

```

## 贡献代码

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/inotify-tools/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/inotify-tools/pulls) 。

## 开源协议

本项目基于 [MIT License](https://gitee.com/openharmony-sig/kcp/blob/master/LICENSE) ，请自由地享受和参与开源。
